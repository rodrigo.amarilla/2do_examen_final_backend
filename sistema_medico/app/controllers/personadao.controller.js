const db = require("../models");

const Personas = db.Personas;

const Op = db.Sequelize.Op;

exports.create = (req, res) => {

// Validate request

    // if (!req.body.factura) {

    //     res.status(400).send({

    //         message: "Debe completar correctamente los datos"

    //     });

    //     return;

    // }

// crea una venta

    
        if (!req.body.tipo_persona) {
         res.status(400).send({
             message: "Debe completar el tipo persona"
         });
        }
        var Persona  = {};
        if(req.body.tipo_persona == 'medico'){
            Persona = {
                nombre: req.body.nombre,
                apellido: req.body.apellido,
                cedula: req.body.cedula,
                email: req.body.email,
                telefono: req.body.telefono,
                fecha_nacimiento: req.body.fecha_nacimiento,
                //para identificar si es paciente o medico
                tipo_persona: req.body.tipo_persona,
                //datos especiales para el tipo persona medico
                especialidad:req.body.especialidad,
                //datos esperaciles para el login de usuario
                usuario: req.body.usuario,
                password: req.body.password
            };
        }
        if(req.body.tipo_persona == 'paciente'){
            Persona = {
                nombre: req.body.nombre,
                apellido: req.body.apellido,
                cedula: req.body.cedula,
                email: req.body.email,
                telefono: req.body.telefono,
                fecha_nacimiento: req.body.fecha_nacimiento,
                //para identificar si es paciente o medico
                tipo_persona: req.body.tipo_persona,
            };
        }


// Guardamos a la base de datos

    Personas.create(Persona)

        .then(data => {

            res.send(data);

        })

        .catch(err => {

            res.status(500).send({

                message:

                    err.message || "Ha ocurrido un error al crear una venta."

            });

        });

};

exports.login = (req, res) => {

    const condition = {
        usuario: req.body.usuario,
        password: req.body.password
    }

    Personas.findAll({ where: condition })

        .then(data => {

            res.send(data);

        })

        .catch(err => {

            res.status(500).send({

                message: "Error al obtener venta con id=" + id

            });

        });

};


exports.findOne = (req, res) => {

    const id = req.params.id;

    Personas.findByPk(id)

        .then(data => {

            res.send(data);

        })

        .catch(err => {

            res.status(500).send({

                message: "Error al obtener venta con id=" + id

            });

        });

};



exports.findAll = (req, res) => {


    var condition = {}
    if(req.query.nombre){
        condition.nombre = { [Op.iLike]: `%${req.query.nombre}%` }
    }

    if(req.query.tipo){
        condition.tipo_persona = req.query.tipo
    }

    console.log("===========");
    console.log(condition);
    console.log("===========");

    Personas.findAll({ where: condition })

        .then(data => {

            res.send(data);

        })

        .catch(err => {

            res.status(500).send({

                message:

                    err.message || "Ocurrio un error al obtener las Personas."

            });

        });

};

//PUT
exports.update = (req,res) => {


    if (!req.body.tipo_persona) {
        res.status(400).send({
            message: "Debe completar el tipo persona"
        });
       }
       var persona  = {};
       if(req.body.tipo_persona == 'medico'){
        persona = {
               nombre: req.body.nombre,
               apellido: req.body.apellido,
               cedula: req.body.cedula,
               email: req.body.email,
               telefono: req.body.telefono,
               fecha_nacimiento: req.body.fecha_nacimiento,
               //para identificar si es paciente o medico
               tipo_persona: req.body.tipo_persona,
               //datos especiales para el tipo persona medico
               especialidad:req.body.especialidad,
               //datos esperaciles para el login de usuario
               usuario: req.body.usuario,
               password: req.body.password
           };
       }
       if(req.body.tipo_persona == 'paciente'){
        persona = {
               nombre: req.body.nombre,
               apellido: req.body.apellido,
               cedula: req.body.cedula,
               email: req.body.email,
               telefono: req.body.telefono,
               fecha_nacimiento: req.body.fecha_nacimiento,
               tipo_persona: req.body.tipo_persona,
           };
       }

    const condicion = {
        where:{
            id:req.body.id
        }
    }


    Personas.update(persona,condicion)
    .then(
        (data) => {
            console.log(data);
            res.send(data);
        }
    ).catch(
        (err) => {
            res.status(500).send({

                message:

                    err.message || "Ha ocurrido un error al actualizar una mesa."

            });
        } 
    );
}

//DELETE
exports.destroy = (req,res) => {


        
        //const id = req.params.id;
        const condicion = {
            where:{
                id:req.params.id
            }
        }


        
    
    
        Personas.destroy(condicion)
        .then(
            (data) => {
                console.log("===========================================================")
                console.log(data);
                res.send({
                    registros_eliminados: data
                });
            }
        ).catch(
            (err) => {
                res.status(500).send({
    
                    message:
    
                        err.message || "Ha ocurrido un error al borrar una mesa."
    
                });
            } 
        );
    

};

