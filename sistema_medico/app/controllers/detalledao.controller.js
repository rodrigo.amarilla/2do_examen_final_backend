const db = require("../models");

const Detalles = db.Detalles;

const Sequelize = db.sequelize;

const { QueryTypes } = require('sequelize');


exports.create = (req, res) => {


    const Detalle = {
        id_ficha: req.body.id_ficha,
        motivo: req.body.motivo,
        diagnostico: req.body.diagnostico,
        tratamiento: req.body.tratamiento,
        fecha: new Date()
    };



    // Guardamos a la base de datos

    Detalles.create(Detalle)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Ha ocurrido un error al crear el detalle."
            });
        });
};


exports.detalles = (req, res) => {

    var id_ficha = req.query.id_ficha;

    console.log(id_ficha);



    Sequelize.query(`
        select 
        f.id as id_ficha,
        f.fecha,
        f.id_paciente ,
        f.id_medico ,
        p.nombre  as nombre_medico,
        p.apellido as apellido_medico,
        p.especialidad ,
        paciente.id as id_paciente,
        paciente.nombre as nombre_paciente,
        paciente.apellido as apellido_paciente,
        d.id as id_detalle,
        d.motivo,
        d.diagnostico ,
        d.tratamiento,
        d.fecha as fecha_detalle
        from "Fichas" f 
        join  "Detalles" d on f.id = d.id_ficha
        join "Personas" p on p.id = f.id_medico 
        join "Personas" paciente  on paciente.id = f.id_paciente 
        `,
        { type: QueryTypes.SELECT }).then(
            (data) => {
                console.log(data);
                res.send(data);
            }
        ).catch(
            (err) => {
                console.log(err);
                res.status(500).send({
                    message: err.message || "Ocurrio un error al obtener los detalles."
                });
            }
        )
}

exports.detallesFicha = (req, res) => {

    var id_ficha = req.query.id_ficha;

    console.log(id_ficha);



    Sequelize.query(`
        select 
        f.id as id_ficha,
        f.fecha,
        f.id_paciente ,
        f.id_medico ,
        p.nombre  as nombre_medico,
        p.apellido as apellido_medico,
        d.id as id_detalle,
        d.motivo,
        d.diagnostico ,
        d.tratamiento,
        d.fecha as fecha_detalle
        from "Fichas" f 
        join  "Detalles" d on f.id = d.id_ficha
        join "Personas" p on p.id = f.id_medico 
        where d.id_ficha = ${id_ficha}
        `,
        { type: QueryTypes.SELECT }).then(
            (data) => {
                console.log(data);
                res.send(data);
            }
        ).catch(
            (err) => {
                console.log(err);
                res.status(500).send({
                    message: err.message || "Ocurrio un error al obtener los detalles."
                });
            }
        )
}