const { log } = require("console");
const db = require("../models");

const Fichas = db.Fichas;

const Op = db.Sequelize.Op;

const Sequelize = db.sequelize;

const { QueryTypes } = require('sequelize');

exports.create = (req, res) => {


            const Ficha = {
                fecha: new Date(),
                id_medico: req.body.id_medico,
                id_paciente: req.body.id_paciente

            };
    


// Guardamos a la base de datos

    Fichas.create(Ficha)

        .then(data => {

            res.send(data);

        })

        .catch(err => {

            res.status(500).send({

                message:

                    err.message || "Ha ocurrido un error al crear una venta."

            });

        });

};


exports.findOne = (req, res) => {

    const id = req.params.id;

    Fichas.findByPk(id)

        .then(data => {

            res.send(data);

        })

        .catch(err => {

            res.status(500).send({

                message: "Error al obtener venta con id=" + id

            });

        });

};



exports.findAll = (req, res) => {

    const nombre = req.query.nombre;
    var condition ={};
    if(req.query.nombre){
        condition.nombre = { [Op.iLike]: `%${nombre}%` };
    }
    if(req.query.id_medico){
        condition.id_medico = req.query.id_medico;
    }
    if(req.query.id_paciente){
        condition.id_paciente = req.query.id_paciente;
    }

    //var condition = nombre ? { nombre: { [Op.iLike]: `%${nombre}%` } } : null;

    Fichas.findAll({ where: condition })

        .then(data => {

            res.send(data);

        })

        .catch(err => {
            console.log(err);
            res.status(500).send({
                message:
                    err.message || "Ocurrio un error al obtener las Fichas."

            });

        });

};

//PUT
exports.update = (req,res) => {


   
     
    const Ficha = {
   
        id_medico: req.body.id_medico,
        id_paciente: req.body.id_paciente,
        id_detalle:req.body.id_detalle

    };


    const condicion = {
        where:{
            id:req.body.id
        }
    }


    Fichas.update(Ficha,condicion)
    .then(
        (data) => {
            console.log(data);
            res.send(data);
        }
    ).catch(
        (err) => {
            res.status(500).send({

                message:

                    err.message || "Ha ocurrido un error al actualizar una mesa."

            });
        } 
    );
}

//DELETE
exports.destroy = (req,res) => {


        
        //const id = req.params.id;
        const condicion = {
            where:{
                id:req.params.id
            }
        }


        
    
    
        Fichas.destroy(condicion)
        .then(
            (data) => {
                console.log("===========================================================")
                console.log(data);
                res.send({
                    registros_eliminados: data
                });
            }
        ).catch(
            (err) => {
                res.status(500).send({
    
                    message:
    
                        err.message || "Ha ocurrido un error al borrar una mesa."
    
                });
            } 
        );
    

};


exports.detalles = (req,res) => {

    var id_paciente   = req.body.id_paciente;
    var id_medico = req.body.id_medico 

    console.log(id_medico)
    console.log(id_paciente)

    Sequelize.query(`
    select 
    f.id as id_ficha,
    f.fecha,
    f.id_paciente ,
    f.id_medico ,
    p.nombre  as nombre_medico,
    p.apellido as apellido_medico,
    d.id as id_detalle,
    d.motivo,
    d.diagnostico ,
    d.tratamiento
    from "Fichas" f 
    left join  "Detalles" d on f.id = d.id_ficha
    join "Personas" p on p.id = f.id_medico 
    where f.id_paciente  = ${id_paciente}
    and f.id_medico = ${id_medico}
    `,
        { type: QueryTypes.SELECT }).then(
            (data)=>{
                console.log(data);
                res.send(data);
            }
        ).catch(
            (err)=>{
                console.log(err);
                res.status(500).send({
                    message:err.message || "Ocurrio un error al obtener los detalles."
                });
            }
        )
}

