module.exports = (sequelize, Sequelize) => {

    const Persona = sequelize.define("Persona", {


        //datos basicos
        id: {

            type: Sequelize.BIGINT,

            primaryKey: true,

            autoIncrement: true

        },

        nombre: {
            type: Sequelize.STRING
        },
        apellido: {
            type: Sequelize.STRING
        },
        cedula: {
            type: Sequelize.STRING,
            unique: true
        },
        email: {
            type: Sequelize.STRING
        },
        telefono: {
            type: Sequelize.STRING
        },
        fecha_nacimiento:{
            type: Sequelize.DATEONLY
        },
        tipo_persona:{
            type: Sequelize.STRING
        },

        //datos especiales para medico
        
        especialidad:{
            type: Sequelize.STRING
        },
        usuario: {
            type: Sequelize.STRING,
            unique: true
        },
        password: {
            type: Sequelize.STRING
        }

    });

    return Persona;

};
