module.exports = (sequelize, Sequelize) => {

    const Ficha = sequelize.define("Ficha", {


        //datos basicos
        id: {

            type: Sequelize.BIGINT,

            primaryKey: true,

            autoIncrement: true

        },

        fecha: {
            type: Sequelize.DATEONLY
        },
        id_paciente:{
            type: Sequelize.BIGINT,
            references:{
                model: 'Personas',
                key:'id'
            },
            allowNull: false
        },
        id_medico:{
            type: Sequelize.BIGINT,
            references:{
                model: 'Personas',
                key:'id'
            },
            allowNull: false
        }

    });

    return Ficha;

};
