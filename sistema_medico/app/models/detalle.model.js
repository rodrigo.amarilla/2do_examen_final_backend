module.exports = (sequelize, Sequelize) => {

    const Detalle = sequelize.define("Detalle", {


        //datos basicos
        id: {

            type: Sequelize.BIGINT,

            primaryKey: true,

            autoIncrement: true

        },
        id_ficha:{
            type: Sequelize.BIGINT,
            references:{
                model: 'Fichas',
                key:'id'
            },
            allowNull: false
        },
        fecha: {
            type: Sequelize.DATEONLY
        },
        motivo: {
            type: Sequelize.STRING
        },
        diagnostico: {
            type: Sequelize.STRING
        },
        tratamiento: {
            type: Sequelize.STRING
        }
       

    });

    return Detalle;

};
