module.exports = app => {

    const detalle = require("../controllers/detalledao.controller.js");

    var router = require("express").Router();

    router.post("/", detalle.create);
    router.get("/ficha/", detalle.detallesFicha);
    router.get("/todos/", detalle.detalles);

    app.use('/sistema_medico/detalle', router);

};
