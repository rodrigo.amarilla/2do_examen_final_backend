module.exports = app => {

    const persona = require("../controllers/personadao.controller.js");

    var router = require("express").Router();

    router.post("/", persona.create);
    router.post("/login", persona.login);

    router.get("/", persona.findAll);
    router.put("/", persona.update);
    router.delete("/:id", persona.destroy);

    router.get("/:id", persona.findOne);

    app.use('/sistema_medico/persona', router);

};
