module.exports = app => {

    const ficha = require("../controllers/fichadao.controller.js");

    var router = require("express").Router();

    router.post("/", ficha.create);
    router.post("/detalles/", ficha.detalles);
    router.get("/", ficha.findAll);
    router.put("/", ficha.update);
    router.delete("/:id", ficha.destroy);

    router.get("/:id", ficha.findOne);

    app.use('/sistema_medico/ficha', router);

};
