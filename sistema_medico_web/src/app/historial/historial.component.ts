import { Component, OnInit } from '@angular/core';
import { ServicioService } from '../services/servicio.service';

@Component({
  selector: 'app-historial',
  templateUrl: './historial.component.html',
  styleUrls: ['./historial.component.css']
})
export class HistorialComponent implements OnInit {

  search:any = ''
  detalles:any = [];
  datasources:any = [];
  displayedColumns: string[] = ['id_ficha', 'id_detalle', 'fecha_detalle', 'nombre_medico',
    'apellido_medico','especialidad','nombre_paciente','apellido_paciente', 'motivo', 'diagnostico', 'tratamiento'];

  constructor(
    private service: ServicioService
  ) {

  }

  ngOnInit(): void {
    this.getDetalles();
  }

  getDetalles() {
    this.service.getDetalles().subscribe(
      (data: any) => {
        console.log(data);
        this.detalles = data;
        const arrayTemp:any = []
        this.detalles.forEach((element:any) => {
          arrayTemp.push(element)
        });
        
        this.datasources = arrayTemp;
        
        
      },
      (err) => {
        console.log(err);
      }
    );
  }

 

  applyFilter(evento:any){
    // console.log(evento);
    console.log(this.search);
    
    const filterValue = (evento.target as HTMLInputElement).value;
    
    this.search = filterValue;
    
    
    this.datasources = this.detalles.filter(
      (item:any) => {
        try {
          var resp = -1;
        resp = item.motivo.toLowerCase( ).indexOf(this.search.toLowerCase( ));
        if(resp > -1){
          return true;
        }
        resp = item.diagnostico.toLowerCase( ).indexOf(this.search.toLowerCase( ));
        if(resp > -1){
          return true;
        }
        resp = item.tratamiento.toLowerCase( ).indexOf(this.search.toLowerCase( ));
        if(resp > -1){
          return true;
        }
        resp = item.nombre_paciente.toLowerCase( ).indexOf(this.search.toLowerCase( ));
        if(resp > -1){
          return true;
        }
        resp = item.apellido_paciente.toLowerCase( ).indexOf(this.search.toLowerCase( ));
        if(resp > -1){
          return true;
        }
        resp = item.nombre_medico.toLowerCase( ).indexOf(this.search.toLowerCase( ));
        if(resp > -1){
          return true;
        }
        resp = item.apellido_medico.toLowerCase( ).indexOf(this.search.toLowerCase( ));
        if(resp > -1){
          return true;
        }
        resp = item.especialidad.toLowerCase().indexOf(this.search.toLowerCase());
        if(resp > -1){
          return true;
        }
        resp = item.fecha_detalle.toLowerCase( ).indexOf(this.search.toLowerCase( ));
        if(resp > -1){
          return true;
        }
        } catch (error) {
          return false;
        }
        
        return false;
        
      }
    );

    
    
    
  }

  

}
