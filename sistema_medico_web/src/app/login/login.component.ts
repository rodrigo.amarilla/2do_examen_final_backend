import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { ServicioService } from '../services/servicio.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
hide: boolean = true;
usuario:any= "";
password:any= "";
error:any = ""
constructor(
  private router: Router,
  private service: ServicioService
){}

login (){
  this.error = "";
  //falta logica para validar usuario 
  console.log(this.password);
  
  
  this.service.login(this.usuario, this.password).subscribe(
    (data:any)=>{
      console.log(data);

      localStorage.setItem('medico',JSON.stringify(data));
      
      if (data.length > 0 ){
        this.router.navigate(['menu']);
      } else {
        this.error = "datos incorrectos";
      }
      
    },
    (err)=>{
      console.log(err);
      this.error = "error de conexion  a servidor"
    }
  ) ;
  
}
}
