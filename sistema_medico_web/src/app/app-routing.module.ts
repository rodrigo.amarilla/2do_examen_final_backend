import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FichasComponent } from './fichas/fichas.component';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { MenuComponent } from './menu/menu.component';
import { HistorialComponent } from './historial/historial.component';




const routes: Routes = [
  { path: 'login', component: LoginComponent },
  {
    path: 'menu', component: MenuComponent,
    children: [
      { path: 'fichas', component: FichasComponent },
      { path: 'historial', component: HistorialComponent }
    ]
},
{ path: '**', redirectTo: '/login' },
  { path: '#/', redirectTo: '/login' }

];


@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
