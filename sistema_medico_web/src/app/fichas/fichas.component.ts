import { Component, OnInit } from '@angular/core';
import { ServicioService } from '../services/servicio.service';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import {map, startWith} from 'rxjs/operators';

@Component({
  selector: 'app-fichas',
  templateUrl: './fichas.component.html',
  styleUrls: ['./fichas.component.css']
})
export class FichasComponent implements OnInit{

  ficha:any={};
  pacientes: any = [];
  medico:any={};
  existeFicha = false;
  errorFicha = '';
  search = '';
  myControl = new FormControl('');
  options: string[] = [];
  filteredOptions: Observable<any[]> = new Observable<any[]>;
  agregando =false;
  detalles:any = [];
  displayedColumns: string[] = ['id_ficha','id_detalle', 'fecha_detalle','nombre_medico',
  'apellido_medico','motivo','diagnostico','tratamiento'];
  existeDetalles = false;

  constructor(
    private service: ServicioService
  ){

  }
  ngOnInit(): void {

    var json = '{}'
    if(localStorage.getItem("medico")){
      json = localStorage.getItem("medico") as string;
    }
    this.medico = JSON.parse(json)[0];
    console.log(this.medico);
    
    this.service.pacientes().subscribe(
      (data)=>{
        console.log(data);
        this.pacientes = data;
        this.pacientes.forEach((element:any) => {
          this.options.push(element.id + ': '+element.nombre+' '+element.apellido);

          this.filteredOptions = this.myControl.valueChanges.pipe(
            startWith(''),
            map((value) => this._filter(value || '')),
          );
          
        });
      },
      (err)=>{
        console.log(err);
        
      }
    )
  }

  private _filter(value: string): string[] {
    console.log(value);
    
    const filterValue = value.toLowerCase();

    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }

  getFicha(){
    this.errorFicha = '';
    this.existeFicha = false;
    
    this.service.getFicha(this.medico.id, this.ficha.id_paciente).subscribe(
      (data:any)=>{
        console.log(data);
        this.detalles  = data;
        if(data.length > 0){
          this.ficha.id_ficha  = data[0].id
          this.existeFicha = true;
          console.log(this.existeFicha);
          this.getDetalles();
        }
      },
      (err)=>{
        console.log(err);
      }
    );
  }

  getDetalles(){
    this.existeDetalles  = false;
    console.log(this.ficha);
    console.log(this.existeFicha);
    this.service.detalles(this.ficha.id_ficha).subscribe(
      (data:any)=>{
        console.log(data );
        console.log(this.existeFicha);
        this.detalles = data;
        if(data.length > 0){
          this.existeDetalles = true;
        }
      },
      (err)=>{
        console.error(err);
      }
    );
  }

  crearFicha(){
    this.errorFicha = '';
    this.service.crearFicha(this.medico.id, this.ficha.id_paciente).subscribe(
      (data:any)=>{
        console.log(data);
          this.existeFicha = true;
          this.ficha.id_ficha = data.id;
      },
      (err)=>{
        console.log(err);
        this.errorFicha = err.error.message;
        console.log(this.errorFicha);
        
      }
    );
  }


  updateMySelection(item:any){
    
    var splitted = item.split(":", 1);
    console.log(splitted);
    this.ficha.id_paciente = splitted[0] as BigInteger;
    console.log(this.ficha);
    this.getFicha();
    
     
  }


  agregar(){
    this.agregando = true;
  }
  cancelarAgregar(){
    this.ficha.motivo = '';
    this.ficha.diagnostico = '';
    this.ficha.tratamiento = '';
    this.agregando = false;
  }

  crearDetalle(){
    const detalle = {
      id_ficha:this.ficha.id_ficha,
      motivo: this.ficha.motivo,
      diagnostico: this.ficha.diagnostico,
      tratamiento: this.ficha.tratamiento
    }
    console.log(detalle);
    this.service.crerDetalle(detalle).subscribe(
      (data)=>{
        console.log(data );
        this.getDetalles();
      },
      (err)=>{
        console.log(err);
      }
    );
  }
}
