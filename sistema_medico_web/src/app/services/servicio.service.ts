import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ServicioService {

  constructor(
    private http: HttpClient
  ) { }

  login(usuario: string, password: string) {
    const datos = {
      usuario: usuario,
      password: password
    }
    return this.http.post(`api/sistema_medico/persona/login`,datos);
  }

  pacientes(){
    return this.http.get(`api/sistema_medico/persona?tipo=paciente`);
  }

  getFicha(id_medico:any, id_paciente:any){
    return this.http.get(`api/sistema_medico/ficha?id_medico=${id_medico}&id_paciente=${id_paciente}`);
  }

  detalles(id_ficha:any){
        return this.http.get(`api/sistema_medico/detalle/ficha?id_ficha=${id_ficha}`);
  }

  crearFicha(id_medico:any, id_paciente:any){
    const datos = {
      id_medico:id_medico,
      id_paciente:id_paciente
    }
    console.log(datos);
    
    return this.http.post(`api/sistema_medico/ficha/`,datos);
  }

  crerDetalle(detalle:any){
    console.log(detalle);
    return this.http.post(`api/sistema_medico/detalle/`,detalle);
  }


  getDetalles(){
    return this.http.get(`api/sistema_medico/detalle/todos`);
  }
}
